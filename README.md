# Paper Prototype of the Celus-5 team
## ImpactMapping
Our ImpactMapping is located in the ImpactMapping.jpg file.

## StoryMapping
Our StoryMapping is located in the StoryMapping.jpg file.

## Architecture
We are going to use the following architecture:
1. Backend For Frontend	- Node.js + Webpack + React
1. Backend				- Flask
1. Database				- MongoDB

It's important to note, that we may change this in the process.

Several possible impovements, that we're thinking about:
1. Implementing Redis caching layer
1. Migrating from MongoDB to PostgreSQL database
1. Serving React webpages on Flask, not on Node.js
